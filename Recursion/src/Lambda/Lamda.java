package Lambda;

public class Lamda
{
    public static void main(final String[] argv) 
    {        
        run((final String s) -> 
        {
            System.out.println("Lambda says: " + s);
        },
        "Hello, world");
    }
    
    private static void run(final Block  block,
                            final String value)
    {
        block.perform(value);
    }
    
    interface Block
    {
        void perform(String s);
    }
}