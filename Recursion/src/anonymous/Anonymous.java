package anonymous;

public class Anonymous
{
    public static void main(final String[] argv) 
    {        
        class LocalNamedBlock
            implements Block
        {
            @Override
            public void perform(final String s) 
            {
                System.out.println("LocalNamedBlock says: " + s);
            }   
        }
        
        final NamedBlock      blockA;
        final InnerNamedBlock blockB;
        final LocalNamedBlock blockC;
        
        blockA = new NamedBlock();
        blockA.perform("Hello, World!");
        
        blockB = new InnerNamedBlock();
        blockB.perform("Hello, World!");
        
        blockC = new LocalNamedBlock();
        blockC.perform("Hello, World!");
        
        new Block()
        {
            @Override
            public void perform(final String s) 
            {
                System.out.println("AnonymousBlock$1 says: " + s);
            }   
        }.perform("Hello, World!");
    }
    
    private static class InnerNamedBlock
        implements Block
    {
        @Override
        public void perform(final String s) 
        {
            System.out.println("InnerNamedBlock says: " + s);
        }   
    }
}

interface Block
{
    void perform(String s);
}

class NamedBlock 
    implements Block
{
    @Override
    public void perform(final String s) 
    {
        System.out.println("NamedBlock says: " + s);
    }   
}
