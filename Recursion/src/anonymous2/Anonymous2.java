package anonymous2;

public class Anonymous2
{
    public static void main(final String[] argv) 
    {        
        class LocalNamedBlock
            implements Block
        {
            @Override
            public void perform(final String s) 
            {
                System.out.println("LocalNamedBlock says: " + s);
            }   
        }
        
        run(new NamedBlock(),      "Hello, world!");
        run(new InnerNamedBlock(), "Hello, world!");
        run(new LocalNamedBlock(), "Hello, world!");
        
        // class Block$1 { /* copy all the stuffs here */ }
        // run(new Block$1(), "Hello, world!");
        run(new Block()
            {
                // all the stuffs
                @Override
                public void perform(final String s) 
                {
                    System.out.println("InnerNamedBlock says: " + s);
                }   
            },
            "Hello, world");
    }
    
    private static void run(final Block  block,
                            final String value)
    {
        block.perform(value);
    }
    
    private static class InnerNamedBlock
        implements Block
    {
        @Override
        public void perform(final String s) 
        {
            System.out.println("InnerNamedBlock says: " + s);
        }   
    }
}

interface Block
{
    void perform(String s);
}

class NamedBlock 
    implements Block
{
    @Override
    public void perform(final String s) 
    {
        System.out.println("NamedBlock says: " + s);
    }   
}