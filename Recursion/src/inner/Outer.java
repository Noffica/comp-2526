package inner;

public class Outer
{
    private int value;
    private final InnerInstance instance;
    
    {
        instance = new InnerInstance(/* this */);
        instance.foo();
    }
    
    private class InnerInstance
    {
        private int value;
        // private Outer Outer.this;
        
        private InnerInstance(/* Outer x */)
        {
            // Outer.this = x;
            System.out.println("InnerInstance");
            Outer.this.value = 7;
            this.value = 8;
            System.out.println(value);
        }
        
        private void foo()
        {
            System.out.println("InnerInstance::foo");
        }
    }
    
    private static class InnerStatic
    {
        private InnerStatic()
        {
            System.out.println("InnerStatic");
//            value = 7;
        }
    }
    
    public static void main(final String[] argv) 
    {
        Outer outer = new Outer();
        
        System.out.println(outer.value);
    }
}
