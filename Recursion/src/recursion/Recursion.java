package recursion;

public class Recursion 
{
    public static void main(String[] args) 
    {
       // countDown(5);
        countUp(5);
    }
    
    private static void countDown(final int a)
    {
        System.out.println(a);
        
        if(a > 0)
        {
            System.out.println("Recursing from " + a);
            countDown(a - 1);
        }
        
        System.out.println("Returning from " + a);
    }
    
    private static void countUp(final int a)
    {
        if (a > 0)
        {
            System.out.println("Recursing from " + a);
            countUp(a - 1);
        }
        
        System.out.println(a);        
        System.out.println("Returning from " + a);
    }
}
