package recursion;

import java.lang.reflect.Method;
import java.util.LinkedList;

public class Reflection 
{
    public static void main(String[] args) 
    {      
//        printClassInformation(Integer.class);
//        printClassInformationRecursive(Boolean.TRUE.getClass());
        printClassInformationRecursive(LinkedList.class);
//        printClassInformation(null);
    }
    
    private static <T> void printClassInformation(final Class<T> clazz)
    {        
        Class<? super T> temp = clazz;
        
        while(temp != null)
        {
            System.out.println(temp.getName());
            temp = temp.getSuperclass();
        }
    }
    
    private static <T> void printClassInformationRecursive(final Class<T> clazz)
    {        
        if (clazz != null)
        {
            final Class<? super T> superclass;
            final Method[]         methods;
        
            System.out.println(clazz.getName());
            methods = clazz.getDeclaredMethods();
            
            for(final Method method : methods)
            {
                System.out.println("\t" + method);
            }
            
            superclass = clazz.getSuperclass();           
            printClassInformationRecursive(superclass);
        }
    }
}
