package reflection;


public class FooA
    implements Foo
{
    @Override
    public void bar()
    {
        System.out.println("FooA::bar");
    }
    
    @Override
    public void car()
    {
        System.out.println("FooA::car");
    }
}
