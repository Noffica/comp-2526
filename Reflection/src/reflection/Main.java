package reflection;


import java.lang.reflect.InvocationTargetException;


public class Main
{
    public static void main(final String[] argv)
    {
        try
        {
            final Foo foo;
        
            foo = FooUtils.makeFoo(argv[0]);
            FooUtils.callMethod(foo, argv[1]);
        }
        catch(final ClassNotFoundException ex)
        {
            System.err.println("Cannot find class " + ex.getLocalizedMessage());
        }
        catch(final InstantiationException ex)
        {
            System.err.println("Cannot create instance of " + ex.getLocalizedMessage());
        }
        catch(final IllegalAccessException ex)
        {
            System.err.println("Cannot access " + ex.getLocalizedMessage());
        }
        catch(final NoSuchMethodException | InvocationTargetException ex)
        {
            System.err.println("Cannot call method " + ex.getLocalizedMessage());
        }
    }    
}
