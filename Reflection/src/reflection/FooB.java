package reflection;


public class FooB
    implements Foo
{
    @Override
    public void bar()
    {
        System.out.println("FooB::bar");
    }
    
    @Override
    public void car()
    {
        System.out.println("FooB::car");
    }
}
