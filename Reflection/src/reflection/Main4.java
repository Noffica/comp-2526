package reflection;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;



public class Main4 
{
    public static void main(final String[] argv)
    {
        try
        {
            final String     fileName;
            final Properties properties;
            final String     className;
            final String     methodName;
            final Foo        foo;
        
            fileName = argv[0];
            
            try(InputStream stream = Main4.class.getClassLoader().getResourceAsStream("reflection/" + fileName))
            {
                properties = new Properties();
                properties.load(stream);
                className = properties.getProperty("className");
                foo = FooUtils.makeFoo(className);
                methodName = properties.getProperty("methodName");
                FooUtils.callMethod(foo, methodName);
            }
            catch(final FileNotFoundException ex)
            {                    
                System.err.println("Cannot open file " + fileName);        
            }
            catch(final IOException ex)
            {                    
                System.err.println("file read errror " + fileName);        
            }
        }
        catch(final ClassNotFoundException ex)
        {
            System.err.println("Cannot find class " + ex.getLocalizedMessage());
        }
        catch(final InstantiationException ex)
        {
            System.err.println("Cannot create instance of " + ex.getLocalizedMessage());
        }
        catch(final IllegalAccessException ex)
        {
            System.err.println("Cannot access " + ex.getLocalizedMessage());
        }
        catch(final NoSuchMethodException | InvocationTargetException ex)
        {
            System.err.println("Cannot call method " + ex.getLocalizedMessage());
        }
    }    
}
