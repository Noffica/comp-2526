package reflection;


import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.util.Scanner;


public class Main2 
{
    public static void main(final String[] argv)
    {
        try
        {
            final String  fileName;
            final File    file;
            final String  className;
            final String  methodName;
            final Foo     foo;
        
            fileName  = argv[0];
            file      = new File(fileName);
            
            try(Scanner scanner = new Scanner(file))
            {
                className = getClassName(scanner);
                foo = FooUtils.makeFoo(className);
                methodName = getMethodName(scanner);
                FooUtils.callMethod(foo, methodName);
            }
            catch(final FileNotFoundException ex)
            {                    
                System.err.println("Cannot open file " + fileName);        
            }
        }
        catch(final ClassNotFoundException ex)
        {
            System.err.println("Cannot find class " + ex.getLocalizedMessage());
        }
        catch(final InstantiationException ex)
        {
            System.err.println("Cannot create instance of " + ex.getLocalizedMessage());
        }
        catch(final IllegalAccessException ex)
        {
            System.err.println("Cannot access " + ex.getLocalizedMessage());
        }
        catch(final NoSuchMethodException | InvocationTargetException ex)
        {
            System.err.println("Cannot call method " + ex.getLocalizedMessage());
        }
    }    
    
    private static String getClassName(final Scanner scanner)
    {
        final String  line;
        final Scanner lineScanner;
        final String  key;
        final String  value;
        
        line        = scanner.nextLine();
        lineScanner = new Scanner(line);        
        lineScanner.useDelimiter("\\s*=\\s*");
        key   = lineScanner.next();
        value = lineScanner.next();
        
        return (value);
    }
    
    private static String getMethodName(final Scanner scanner)
    {
        final String  line;
        final Scanner lineScanner;
        final String  key;
        final String  value;
        
        line        = scanner.nextLine();
        lineScanner = new Scanner(line);        
        lineScanner.useDelimiter("\\s*=\\s*");
        key   = lineScanner.next();
        value = lineScanner.next();
        
        return (value);
    }
}
