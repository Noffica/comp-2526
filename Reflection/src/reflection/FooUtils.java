package reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 *
 * @author darcy
 */
public final class FooUtils
{
    private FooUtils()
    {
        throw new IllegalStateException("Cannot create an instance of FooUtils");
    }
    
    static Foo makeFoo(final String name) 
        throws ClassNotFoundException, 
               InstantiationException, 
               IllegalAccessException
    {
        final Class<Foo> fooClass;
        final Foo        foo;
        
        fooClass = (Class<Foo>) Class.forName("reflection." + name);
        foo      = fooClass.newInstance();
        
        return foo;
    }

    static void callMethod(final Foo foo, final String methodName) 
        throws NoSuchMethodException, 
               IllegalAccessException, 
               InvocationTargetException
    {
        final Class<Foo> clazz;
        final Method     method;
        
        clazz  = (Class<Foo>)foo.getClass();
        method = clazz.getMethod(methodName);
        method.invoke(foo, new Object[]{});
    }    
}
