package reflection;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Scanner;


public class Main3 
{
    public static void main(final String[] argv)
    {
        try
        {
            final String  fileName;
            final File    file;
            final String  className;
            final String  methodName;
            final Foo     foo;
        
            fileName  = argv[0];
            file      = new File(fileName);
            
            try(BufferedReader reader = new BufferedReader(new FileReader(file)))
            {
                className = getClassName(reader);
                foo = FooUtils.makeFoo(className);
                methodName = getMethodName(reader);
                FooUtils.callMethod(foo, methodName);
            }
            catch(final FileNotFoundException ex)
            {                    
                System.err.println("Cannot open file " + fileName);        
            }
            catch(final IOException ex)
            {                    
                System.err.println("file read errror " + fileName);        
            }
        }
        catch(final ClassNotFoundException ex)
        {
            System.err.println("Cannot find class " + ex.getLocalizedMessage());
        }
        catch(final InstantiationException ex)
        {
            System.err.println("Cannot create instance of " + ex.getLocalizedMessage());
        }
        catch(final IllegalAccessException ex)
        {
            System.err.println("Cannot access " + ex.getLocalizedMessage());
        }
        catch(final NoSuchMethodException | InvocationTargetException ex)
        {
            System.err.println("Cannot call method " + ex.getLocalizedMessage());
        }
    }    
    
    private static String getClassName(final BufferedReader reader)
        throws IOException
    {
        final String  line;
        final Scanner lineScanner;
        final String  key;
        final String  value;
        
        line        = reader.readLine();
        lineScanner = new Scanner(line);        
        lineScanner.useDelimiter("\\s*=\\s*");
        key   = lineScanner.next();
        value = lineScanner.next();
        
        return (value);
    }
    
    private static String getMethodName(final BufferedReader reader)
        throws IOException
    {
        final String  line;
        final Scanner lineScanner;
        final String  key;
        final String  value;
        
        line        = reader.readLine();
        lineScanner = new Scanner(line);        
        lineScanner.useDelimiter("\\s*=\\s*");
        key   = lineScanner.next();
        value = lineScanner.next();
        
        return (value);
    }
}
