package linkedList;

public class SingleLinkedList<T> //generics. Now multiple types of Objects can be used.
{

    private Node<T> head;

    private static class Node<T> //generics
    {
        private         Node<T> next;   //now many types of Objects can be used
        private final   T       value;  //generics

        public Node (final T val) {
            value = val;
        }
    }

    public void add (final T val) { //we can now use various types of Objects
        final Node<T> newNode = new Node(val);

        if ( head == null ) {
            head = newNode;
        }
        else {
            Node<T> tempNode = head;

            while ( tempNode.next != null ) {
                tempNode = tempNode.next;
            }

            tempNode.next = newNode;
        }
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();

        Node<T> tempNode = head;

        while ( tempNode != null ) {
            builder.append(tempNode.value);
            builder.append(" -> ");

            tempNode = tempNode.next;
        }

        return builder.toString();
    }

    public int size() {
        int size = 0;

        if ( head == null ) {
            return size;
        }
        else {
            Node<T> tempNode = head;

            while ( tempNode.next != null ) {
                size++;
            }
        }

        return size;
    }

    public boolean contains (final T entityToSearch) {
        boolean contains = false;
        Node<T> tempNode = head;

        while ( tempNode != null ) {
            if ( tempNode.value.equals(entityToSearch) ) {
                contains = true;
            }

            tempNode = tempNode.next;
        }

        return contains;
    }

} // end of class
