package arithmetic;

public class AdditionOperation
    implements Operation
{

    public double perform (double operandA, double operandB) {
        return ( operandA + operandB );
    }

} //end of file
