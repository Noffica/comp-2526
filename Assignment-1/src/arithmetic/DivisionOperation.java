package arithmetic;

public class DivisionOperation
    implements Operation
{

    public double perform (double operandA, double operandB) {
        return ( (double) operandA / (double) operandB );
    }

} //end of file
