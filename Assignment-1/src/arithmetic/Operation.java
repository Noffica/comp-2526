package arithmetic;

public interface Operation
{
    double perform (double operandA, double operandB);
}
