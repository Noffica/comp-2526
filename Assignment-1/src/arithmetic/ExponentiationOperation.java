package arithmetic;

public class ExponentiationOperation
    implements Operation
{

    public double perform (double operandA, double operandB) {
        return (Math.pow(operandA, operandB));
    }

} //end of file
