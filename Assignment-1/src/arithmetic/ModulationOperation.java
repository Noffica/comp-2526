package arithmetic;

public class ModulationOperation
    implements Operation
{

    public double perform (double operandA, double operandB) {
        return ( operandA % operandB );
    }

} //end of file
