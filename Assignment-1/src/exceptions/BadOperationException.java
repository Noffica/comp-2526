package exceptions;

public class BadOperationException
    extends Exception
{

    // instance variable(s)
    private String message;


    // Constructor
    public BadOperationException (String message) {
        super(message);
        this.message = message;
    }

    // method
    public String getName() {
        return this.message;
    }

    // method
    public String getMessage() {
        return ("Bad Operation: " + this.message);
    }

} //end of file
