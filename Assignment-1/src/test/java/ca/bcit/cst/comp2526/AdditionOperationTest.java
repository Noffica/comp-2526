package test.java.ca.bcit.cst.comp2526;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import arithmetic.*;

public class AdditionOperationTest
{
    @Test
    public void testPerform()
    {
        final AdditionOperation operation;
        
        operation = new AdditionOperation();
        assertEquals(0, operation.perform(0, 0), 0.01);
        assertEquals(0, operation.perform(1, -1), 0.01);
        assertEquals(1, operation.perform(0, 1), 0.01);
        assertEquals(1, operation.perform(1, 0), 0.01);
        assertEquals(2, operation.perform(1, 1), 0.01);
        assertEquals(2, operation.perform(0, 2), 0.01);
        assertEquals(2, operation.perform(2, 0), 0.01);
    }
}
