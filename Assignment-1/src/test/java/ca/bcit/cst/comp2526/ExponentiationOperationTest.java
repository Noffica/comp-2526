package test.java.ca.bcit.cst.comp2526;

import arithmetic.ExponentiationOperation;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExponentiationOperationTest
{
    @Test
    public void testPerform()
    {
        final ExponentiationOperation operation;

        operation = new ExponentiationOperation();
        assertEquals(1, operation.perform(1, 1), 0.01);
        assertEquals(1, operation.perform(1, 2), 0.01);
        assertEquals(2, operation.perform(2, 1), 0.01);
        assertEquals(4, operation.perform(2, 2), 0.01);
        assertEquals(8, operation.perform(2, 3), 0.01);
        assertEquals(16, operation.perform(2, 4), 0.01);
        assertEquals(32, operation.perform(2, 5), 0.01);
        assertEquals(64, operation.perform(2, 6), 0.01);
        assertEquals(128, operation.perform(2, 7), 0.01);
        assertEquals(256, operation.perform(2, 8), 0.01);
        assertEquals(512, operation.perform(2, 9), 0.01);
        assertEquals(1024, operation.perform(2, 10), 0.01);
    }
}
