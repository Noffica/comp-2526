package test.java.ca.bcit.cst.comp2526;


import arithmetic.MultiplicationOperation;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class MultiplicationOperationTest
{
    @Test
    public void testPerform()
    {
        final MultiplicationOperation operation;

        operation = new MultiplicationOperation();
        assertEquals(0, operation.perform(0, 0), 0.01);
        assertEquals(-1, operation.perform(1, -1), 0.01);
        assertEquals(0, operation.perform(0, 1), 0.01);
        assertEquals(0, operation.perform(1, 0), 0.01);
        assertEquals(1, operation.perform(1, 1), 0.01);
        assertEquals(0, operation.perform(0, 2), 0.01);
        assertEquals(6, operation.perform(1, 6), 0.01);
        assertEquals(10, operation.perform(2, 5), 0.01);
        assertEquals(12, operation.perform(3, 4), 0.01);
        assertEquals(12, operation.perform(4, 3), 0.01);
        assertEquals(10, operation.perform(5, 2), 0.01);
        assertEquals(6, operation.perform(6, 1), 0.01);
    }
}
