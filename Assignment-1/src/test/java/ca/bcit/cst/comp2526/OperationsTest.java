package test.java.ca.bcit.cst.comp2526;


import arithmetic.*;
import exceptions.*;
import utilities.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;


public class OperationsTest
{
    @Test
    public void testGetOperationClassFor()
    {
        assertEquals(AdditionOperation.class,       Operations.getOperationClassFor("+"));
        assertEquals(SubtractionOperation.class,    Operations.getOperationClassFor("-"));
        assertEquals(MultiplicationOperation.class, Operations.getOperationClassFor("*"));
        assertEquals(DivisionOperation.class,       Operations.getOperationClassFor("/"));
        assertEquals(ModulationOperation.class,     Operations.getOperationClassFor("%"));
        assertEquals(ExponentiationOperation.class, Operations.getOperationClassFor("^"));
        assertEquals(null,                          Operations.getOperationClassFor("!"));
    }

    @Test
    public void testGetOperationFor()
        throws InstantiationException,
               IllegalAccessException,
               BadOperationException
    {
        assertEquals(AdditionOperation.class,       Operations.getOperationFor("+").getClass());
        assertEquals(SubtractionOperation.class,    Operations.getOperationFor("-").getClass());
        assertEquals(MultiplicationOperation.class, Operations.getOperationFor("*").getClass());
        assertEquals(DivisionOperation.class,       Operations.getOperationFor("/").getClass());
        assertEquals(ModulationOperation.class,     Operations.getOperationFor("%").getClass());
        assertEquals(ExponentiationOperation.class, Operations.getOperationFor("^").getClass());

        try
        {
            Operations.getOperationFor("!");
            fail("getOperationFor unknown operation type must throw a BadOperationException");
        }
        catch(final BadOperationException ex)
        {
            assertEquals("!", ex.getName());
            assertEquals("Bad Operation: !", ex.getMessage());
        }
    }
}
