package test.java.ca.bcit.cst.comp2526;

import exceptions.BadOperationException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BadOperationExceptionTest
{
    @Test
    public void testParent()
    {
        // BadOperation must extend Exception
        assertEquals(Exception.class, BadOperationException.class.getSuperclass());
    }

    @Test
    public void testGetName()
    {
        assertEquals("Foo", new BadOperationException("Foo").getName());
        assertEquals("Foo Bar", new BadOperationException("Foo Bar").getName());
    }

    @Test
    public void testGetMessage()
    {
        assertEquals("Bad Operation: Foo", new BadOperationException("Foo").getMessage());
        assertEquals("Bad Operation: Foo Bar", new BadOperationException("Foo Bar").getMessage());
    }
}
