package test.java.ca.bcit.cst.comp2526;


import arithmetic.ModulationOperation;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ModulationOperationTest
{
    @Test
    public void testPerform()
    {
        final ModulationOperation operation;

        operation = new ModulationOperation();
        assertEquals(Double.NaN, operation.perform(0, 0), 0.01);
        assertEquals(0, operation.perform(1, 1), 0.01);
        assertEquals(1, operation.perform(1, 2), 0.01);
        assertEquals(Double.NaN, operation.perform(2, 0), 0.01);
        assertEquals(0, operation.perform(2, 1), 0.01);
        assertEquals(0, operation.perform(2, 2), 0.01);
        assertEquals(2, operation.perform(2, 3), 0.01);
        assertEquals(2, operation.perform(2, 4), 0.01);
    }
}
