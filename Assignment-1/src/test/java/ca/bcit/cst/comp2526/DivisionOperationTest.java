package test.java.ca.bcit.cst.comp2526;


import arithmetic.DivisionOperation;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class DivisionOperationTest
{
    @Test
    public void testPerform()
    {
        final DivisionOperation operation;

        operation = new DivisionOperation();
        assertEquals(1, operation.perform(1, 1), 0.01);
        assertEquals(0.5, operation.perform(1, 2), 0.01);
        assertEquals(2, operation.perform(2, 1), 0.01);
    }
}
