package utilities;

import arithmetic.*;
import exceptions.BadOperationException;

import java.util.HashMap;
import java.util.Map;

public class Operations
{

    private static final Map<String, Class<? extends Operation>> operations;

    static {
        HashMap<String, Class<? extends Operation>> hashOfOperators = new HashMap<>();

        try {
            hashOfOperators.put("+", (Class<? extends Operation>) Class.forName("arithmetic.AdditionOperation"));
            hashOfOperators.put("-", (Class<? extends Operation>) Class.forName("arithmetic.SubtractionOperation"));
            hashOfOperators.put("*", (Class<? extends Operation>) Class.forName("arithmetic.MultiplicationOperation"));
            hashOfOperators.put("/", (Class<? extends Operation>) Class.forName("arithmetic.DivisionOperation"));
            hashOfOperators.put("%", (Class<? extends Operation>) Class.forName("arithmetic.ModulationOperation"));
            hashOfOperators.put("^", (Class<? extends Operation>) Class.forName("arithmetic.ExponentiationOperation"));
        }
        catch (ClassNotFoundException e) {
            e.getLocalizedMessage();
        }

        operations = hashOfOperators;
    }

    public static Operation getOperationFor (String operator) throws BadOperationException {
        Operation operationObject = null;

        try {
            operationObject = operations.get(operator).newInstance();
        } catch (NullPointerException e) {
            throw new BadOperationException(operator);
        } catch (InstantiationException f) {
            f.getLocalizedMessage();
        } catch (IllegalAccessException g) {
            g.getLocalizedMessage();
        }

        return operationObject;
    }

    public static Class<? extends Operation> getOperationClassFor (String operator) {
        return operations.get(operator);
    }

} //end of file
