package executables;

import arithmetic.*;
import exceptions.*;
import utilities.*;

public class Main
{
    public static void main(final String[] argv) {
        Operation operation = null;

        try {
            operation = Operations.getOperationFor(argv[1]);
        }
        catch (BadOperationException e) {
            e.getLocalizedMessage();
        }
//        the following exceptions are disabled since they are not thrown under `try`
//        catch (InstantiationException f) {
//            f.getLocalizedMessage();
//        }
//        catch (IllegalAccessException g) {
//            g.getLocalizedMessage();
//        }
        System.out.println(
                operation.perform(
                    Double.parseDouble(argv[0]),
                    Double.parseDouble(argv[2])
                )
        );
    }
} //end of file
