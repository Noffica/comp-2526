package linkedlist;

public class SingleLinkedList<T>
{
    private Node<T> head;

    private static class Node<T>
    {
        private Node<T> next;
        private final T value;
        
        Node(final T val)
        {            
            value = val;
        }
    }
    
    public void add(final T val)
    {
        final Node<T> newNode;
        
        newNode = new Node<>(val);
        
        if (head == null)
        {
            head = newNode;
        }
        else
        {
            Node<T> tempNode;
            
            tempNode = head;
            
            while (tempNode.next != null)
            {
                tempNode = tempNode.next;
            }
   
            tempNode.next = newNode;
        }
    }
    
    @Override
    public String toString()
    {
        final StringBuilder builder     = new StringBuilder();
        Node<T>             tempNode    = head;
        
        while (tempNode != null)
        {
            builder.append(tempNode.value);
            builder.append(" -> ");
            
            tempNode = tempNode.next;
        }
        
        return builder.toString();
    }
    
    public int size()
    {
        int count        = 0;
        Node<T> tempNode = head;
        
        while(tempNode != null)
        {
            count++;
            tempNode = tempNode.next;
        }
        
        return count;
    }
    
    public boolean contains(final T searchFor)
    {
        boolean retVal   = false;
        Node<T> tempNode = head;
        
        while(tempNode != null)
        {
            if (tempNode.value.equals(searchFor))
            {
                retVal = true;
                break;
            }
            
            tempNode = tempNode.next;
        }
        
        return retVal;
    }

/*
        Node<T> tempNode;
        
        tempNode = head;
        
        while(tempNode != null)
        {
            tempNode = tempNode.next;
        }
*/
}
