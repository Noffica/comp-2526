package linkedlist;

public class Main
{
    public static void main(final String[] args)
    {
        final SingleLinkedList<String> list;
        final SingleLinkedList<Integer> list2;
        
        list  = new SingleLinkedList<>();        
        list2 = new SingleLinkedList<>();        

        System.out.println(list);
        System.out.println(list.size()); // 0

        list.add("A");        
        list2.add(5);
        System.out.println(list);
        System.out.println(list.size()); // 1
        System.out.println(list2);
        System.out.println(list2.size()); // 1

        list.add("B");        
        System.out.println(list);
        System.out.println(list.size()); // 2

        list.add("C");        
        System.out.println(list);
        System.out.println(list.size()); // 3

        list.add("D");        
        System.out.println(list);
        System.out.println(list.size()); // 4

        list.add("E");        
        System.out.println(list);
        System.out.println(list.size()); // 5

        list.add("F");        
        System.out.println(list);
        System.out.println(list.size()); // 6
        
        find(list, "A");
        find(list, "B");
        find(list, "C");
        find(list, "D");
        find(list, "E");
        find(list, "F");
        find(list, "G");
        find(list2, 0);
        find(list2, 5);
    }

    private static <T> void find(
        final SingleLinkedList<T> list,
        final T searchFor
    )
    {
        if (list.contains(searchFor))
        {
            System.out.println("found " + searchFor);
        }
        else
        {
            System.out.println("did not find " + searchFor);
        }
    }
}


