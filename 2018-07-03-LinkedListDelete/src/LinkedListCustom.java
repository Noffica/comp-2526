public class LinkedListCustom<T>
{
    // instance variables
    private Integer size;
    private Node    first;

    // an inner class
    private class Node
    {
        // instance variables
        public T    data;
        public Node next;
    }

    // LinkedListCustom constructor
    public LinkedListCustom() {
        first = null;
    }

    public void insert (Integer index, T element) {
        Node tempNode = first;
        Node newNode  = new Node();
        newNode.data = element;

        if (size == null || size == 0) {
            newNode.next = null;
            first = newNode;
        }
        else {
            Node secondTemp = null;

            for (int i = 0; (i <= index - 1) && (tempNode.next != null); i++) {
                tempNode = tempNode.next;
                secondTemp = tempNode;
            }

            tempNode.data = element;
            tempNode.next = secondTemp;
        }

        size++;
    }

} //end of class
