class Main4
{
    public static void main (String[] args)
    {
        final H h;

        h = new I();
    }
}

class H
{
    public H(int x) {
        System.out.println("H()");
    }
}

class I
    extends H
{
    public I()
    {
        super(4);
        System.out.println("I()");
    }
}
