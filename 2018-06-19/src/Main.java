import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Main
{
    public static void main (String[] args)
    {
        final List<String>  words;
        final Scanner       scanner;

        scanner = createScanner();
        words   = new LinkedList<>();

        while ( scanner.hasNext() )
        {
            final String word;

            word = scanner.next();
            words.add(word);
        }

        for ( final String word : words )
        {
            System.out.println(word);
        }
    }

    private static Scanner createScanner()
    {
        final Scanner scanner;

        scanner = new Scanner("Hello evil world");

        return scanner;
    }
}
