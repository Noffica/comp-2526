public class A
{
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        foo();
    }

    private static void foo()
    {
        try {
            bar();
        }
        catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

    private static void bar() throws Exception
    {
        throw new Exception();
    }
}
