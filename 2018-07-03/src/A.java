public class A
    implements Foo
{
    private int value = 3;

    public int addTo(Foo val) {
        return (val.getValue() + value);
    }

    public int getValue() {
        return value;
    }
}
