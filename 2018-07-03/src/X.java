public class X
{
    public static <T extends Foo> int foo(T a, T b) {
        return a.addTo(b);
    }
}
